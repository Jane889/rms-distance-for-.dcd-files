# sets the x and y range of the data set.
set xrange [1000:1800]
set yrange [1.00:1.35]

# setting title, x and y labels
set title 'rmsd of backbone for first 20ns of ST8' 
set xlabel 'frames'; set ylabel 'rmsd'

# removes the random legend. Since I only have one data set, I don't need it
unset key

# calculates the average rmsd from the data set above.
f(x) = mean_y
fit f(x) 'rmsd_backbone_first_20ns_nvt_st8.txt' u 1:2 via mean_y

# calculates the standard deviation
stddev_y = sqrt(FIT_WSSR / (FIT_NDF + 1 ))

# sets the file type being saved to jpeg
set terminal jpeg

# sets the file name to rmsd_backbone_first_20ns_nvt_st8.jpeg
set output 'rmsd_backbone_first_20ns_nvt_st8_1000-1800.jpeg'

# prints the mean value on the graph at position x=1050 and y=1.32
set label 1 gprintf("Mean = %g", mean_y) at 1050, 1.32

# prints the standard deviation on the graph at position x = 1050 and y = 1.30
set label 2 gprintf("Stdev = %g", stddev_y) at 1050, 1.30
# plot the graph with lines.
plot 'rmsd_backbone_first_20ns_nvt_st8.txt' w l

